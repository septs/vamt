# VAMT 3.1

The VAMT is **V**olume **A**ctivation **M**anagement **T**ool

## References

- [Install and configure VAMT](https://learn.microsoft.com/en-us/windows/deployment/volume-activation/install-configure-vamt)
- [PowerShell Module](https://learn.microsoft.com/powershell/module/vamt/)
- [Technical Reference](https://learn.microsoft.com/en-us/windows/deployment/volume-activation/volume-activation-management-tool)
- [Implementing and Activating](https://www.terminalworks.com/blog/post/2015/12/13/volume-activation-management-tool-3-1-implementing-and-activating)
- [Online License Checker](https://dbmer.com/checkkey/)
